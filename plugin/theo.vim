if exists("g:loaded_theo")
	finish
endif
let g:loaded_theo = 1

lua theo = require('theo')

command! -nargs=0 Theo lua theo.toggle('vs')
command! -nargs=0 Tveo lua theo.toggle('sp')
