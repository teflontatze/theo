--[[ Local state ]]--

local api = vim.api

-- initialise to -1 to indicate first use
local win_id = -1 
local buf_id = -1 

local function theo_has_window()
	return api.nvim_win_is_valid(win_id)
end

local function theo_has_buffer()
	return api.nvim_buf_is_valid(buf_id)
end

local function theo_open(split_cmd)
	-- Open new window according to split_cmd
	vim.cmd(split_cmd)
	-- Reference for closing window later
	win_id = vim.fn.win_getid()

	-- Set window local options
	api.nvim_win_set_option(win_id, 'number', false)

	if theo_has_buffer() then
		-- We have a valid buffer so we use it
		api.nvim_win_set_buf(win_id, buf_id)
	else
		-- Create a terminal buffer
		vim.cmd('terminal')
		-- And keep track of it
		buf_id = vim.fn.bufnr()
		-- Set buffer local options
		api.nvim_buf_set_option(buf_id, 'buflisted', false)
	end

	-- Enter insert mode
	vim.cmd('startinsert')
end

local function theo_close()
	api.nvim_win_close(win_id, true)
end

local function theo_toggle(split_cmd)
	if theo_has_window() then
		theo_close()
	else
		theo_open(split_cmd)
	end
end

return {
	toggle = theo_toggle
}
